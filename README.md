### Comment configurer un projet symfony avec React js

Lien utile:
https://www.cloudways.com/blog/symfony-react-using-webpack-encore/

1. Créer le projet symfony
```
symfony new project --full
```

2. Run le server symfony
```
cd project
symfony serve
```

3. Installer Webpack Encore
```
composer require symfony/webpack-encore-bundle
```

4. Installer React Js et Babel
```
yarn add --dev react react-dom prop-types babel-preset-react
```

5. Verifier si yarn est bien installer
```
yarn install
```

6. Dans le fichier ./webpack.config.js, activer react
```
// webpack.config.js
// ...
 Encore
     // ...
     .enableReactPreset()
     // uncomment to define the assets of the project
     .addEntry('js/app', './assets/js/app.js');
```

7. Créer un controller default
```
php bin/console make:controller Default
```
Le rendu sera alors ainsi
```
// ./src/Controller/DefaultController.php
 <?php
 
 namespace App\Controller;
 
 use Symfony\Component\Routing\Annotation\Route;
 use Symfony\Bundle\FrameworkBundle\Controller\Controller;
 
 class DefaultController extends Controller
 {
     /**
      * @Route("/", name="default")
      */
     public function indexAction()
     {
         return $this->render('default/index.html.twig');
 
     }
 }
```

8. Créer AppComponent dans le fichier app.js
```
import React from 'react';
 import ReactDOM from 'react-dom';
 
 import Items from './Components/Items';
 
 
 class App extends React.Component {
    state = {
      entries: []
    }
 
     componentDidMount() {
         fetch('https://jsonplaceholder.typicode.com/posts/')
             .then(response => response.json())
             .then(entries => {
                 this.setState({
                     entries
                 });
             });
     }
 
     render() {
         return (
             <div className="row">
                 {this.state.entries.map(
                     ({ id, title, body }) => (
                         <Items
                             key={id}
                             title={title}
                             body={body}
                         >
                         </Items>
                     )
                 )}
             </div>
         );
     }
 }
 
 ReactDOM.render(<App />, document.getElementById('root'));
 ```

 9. Créer l'items component

 ```
 // ./assets/js/components/Items.js
 import React from 'react';
 
 const Items = ({ id, title, body }) => (
     <div key={id} className="card col-md-4" style={{width:'200'}}>
         <div className="card-body">
             <p>{id}</p>
             <h4 className="card-title">{title}</h4>
             <p className="card-text">{body}</p>
             <a href="https://jsonplaceholder.typicode.com/posts/" className="btn btn-primary">More Details</a>
         </div>
     </div>
 );
 
 export default Items;
```

10. Afficher le contenu du composant react dans le fichier twig
```
./templates/default/index.html.twig
 
 {% extends 'base.html.twig' %}
 
 {% block title %} Symfony React Starter {% endblock %}
 
 {% block body %}
     <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
         <a class="navbar-brand" href="#"> Symfony React Starter </a>
     </nav>
     
     <div id="root"></div>
   
 {% endblock %}
 ```

11. Ce qui suit n'est pas necessaire dans la procedure effectué
```
<!DOCTYPE html>
 <html>
     <head>
         <meta charset="UTF-8">
         <title>{% block title %}Welcome!{% endblock %}</title>
         {% block stylesheets %}{% endblock %}
         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
     </head>
     <body>
         {% block body %}{% endblock %}
         {% block javascripts %}
         
         <!-- Include the compiled script-->
             <script type="text/javascript" src="{{ asset('build/js/app.js') }}"></script>
             
         {% endblock %}
     </body>
 </html>
 ```

12. Installation de bootstrap
```
yarn add bootstrap@next
```

13. Installation de poppersjs
```
yarn add @poppersjs/core
```

14. Mettre à jour
```
yarn install
```

15. Installation de sass loader
```
yarn add sass-loader@^12.0.0 sass --dev  
```

16. Installation de postcss loader autoprefixer
```
yarn add postcss-loader autoprefixer --dev
```

17. Renommer le fichier app.css -> app.scss et faire le necessaire dans les autres fichiers pour que cela soit pris en compte

18. Dans le fichier app.scss ajouter ceci
```
@import "~bootstrap/scss/bootstrap";
```

19. Dans le fichier base.html.twig ajouter ces scripts
```
Dans le header ?
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
```

20. Dans le fichier app.js
```
import { Tooltip, Toast, Popover } from 'bootstrap';
```



